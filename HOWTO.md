mkdir docker/data

sudo cp $NANO_DATA_LOCATION ./docker/data/RaiBlocks

sudo cp -RL /etc/letsencrypt/live/$DOMAIN/* docker/data/certs

cp canoed.conf.example canoed.conf

cd docker

docker-compose build && docker-compose up -d


NAO ESQUECER de configurar o nanonode:


{
    "rpc_enable": "true",
    ...

    "rpc": {
        ...
        "enable_control": "true",
        ...
     },
     "node": {
        ...
        "callback_address": "canoed_node",
        "callback_port": "8180",
        "callback_target": "/callback",
	...
      }
      ...
}


Não há necessidade de rodar o script para inicializar o canoed pois as configurações iniciais do banco estão sendo feitas pelo container do Postgres devidamente configurado para comportar o canoed ;)