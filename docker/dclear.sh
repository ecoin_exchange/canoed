#!/bin/zsh

docker-compose stop
docker-compose rm -f
docker rmi docker_mqtt docker_app docker_nginx docker_postgres

docker-compose up -d --build --force-recreate
