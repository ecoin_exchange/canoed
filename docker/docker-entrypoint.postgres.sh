#!/bin/bash
set -e

#    CREATE USER $POSTGRES_USER;
#    CREATE DATABASE $POSTGRES_DB;
#    GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER;
#    \password

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    \connect $POSTGRES_DB
    CREATE EXTENSION pgcrypto;
    CREATE TABLE IF NOT EXISTS block_timestamp (
        hash varchar(64) NOT NULL,  
        timestamp timestamp default current_timestamp,  
        PRIMARY KEY (hash)  
      );
    CREATE TABLE IF NOT EXISTS vmq_auth_acl
    (
        mountpoint character varying(10) NOT NULL,
        client_id character varying(128) NOT NULL,
        username character varying(128) NOT NULL,
        password character varying(128),
        publish_acl json,
        subscribe_acl json,
        CONSTRAINT vmq_auth_acl_primary_key PRIMARY KEY (mountpoint, client_id, username)
    );
    INSERT INTO vmq_auth_acl
        (mountpoint, client_id, username, password, publish_acl, subscribe_acl)
    VALUES (
        '',
        '$MQTT_USER',
        '$MQTT_USER',
        crypt('$MQTT_PASS', gen_salt('bf')),
        '[{"pattern": "#"}]',
        '[{"pattern": "#"}]'
    )
    ON CONFLICT DO NOTHING;
    \q
EOSQL
